package model;

import entity.UserEntity;
import front.utils.ConnectionManager;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Authentication;
import utils.Configuration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by thinh on 9/1/2016.
 */
public class ModelUser {
    private static final Logger _log = Logger.getLogger(ModelUser.class);
    private static final Lock createLock_ = new ReentrantLock();
    private static ModelUser _instance = null;

    public static final String _database = Configuration._MYSQL_DATABASE_NAME;

    public static ModelUser getInstance() {

        if (_instance == null) {
            createLock_.lock();
            try {
                if (_instance == null) {
                    _instance = new ModelUser();
                }
            } finally {
                createLock_.unlock();
            }
        }
        return _instance;
    }

    public ModelUser() {
    }
    public Map<String, UserEntity> getMapActiveUsers() {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<String, UserEntity> mapUser = new HashMap<>();

        try {

            conn = ConnectionManager.getInstance().getConnection();

            if (conn == null) {
                return null;
            }

            stmt = conn.prepareStatement("SELECT * FROM user");

            stmt.setInt(1, Configuration.ACTIVED_USER);

            rs = stmt.executeQuery();

            while (rs.next()) {
                UserEntity ent = new UserEntity(rs);
                mapUser.put(ent.userName, ent);
            }

        } catch (Exception ex) {
            _log.error("[ERROR] Error when getMapActiveUsers", ex);
        } finally {
            ConnectionManager.closeResultSet(rs);
            ConnectionManager.closeStatement(stmt);
            ConnectionManager.closeConnection(conn);
        }

        return mapUser;
    }

}
