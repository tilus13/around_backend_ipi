/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package front.utils;

/**
 *
 * @author tuantq2
 */

import org.apache.log4j.Logger;
import utils.DBPool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionManager {

    private DBPool dbPool;

    private static ConnectionManager _instance = null;
    private static final Lock createLock_ = new ReentrantLock();
    private static final Logger _log = Logger.getLogger(ConnectionManager.class);

    public static ConnectionManager getInstance() {

        if (_instance == null) {
            createLock_.lock();
            try {
                if (_instance == null) {
                    _instance = new ConnectionManager();
                }
            } finally {
                createLock_.unlock();
            }
        }
        return _instance;
    }

    public ConnectionManager() {
        try {

            dbPool = DBPool.getInstance();

        } catch (Exception e) {
            _log.info(e.toString());
        }
    }

    public Connection getConnection() {

        Connection conn = null;
        try {
            conn = dbPool.getConn();

        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }
        return conn;

    }

    public static void closeConnection(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }

    }

    public static void closeStatement(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }

    }

    public static void closeResultSet(ResultSet rSet) {
        try {
            if (rSet != null) {
                rSet.close();
            }
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }

    }

    public void shutdownConnPool() {

        try {
            if (dbPool != null) {
                dbPool.shuttdownPool();
            }

            
            
           

        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }
    }
}
