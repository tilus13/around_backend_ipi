/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package front.servlet;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author thinh
 */
public class BaseServlet extends HttpServlet {

    private static final Logger _log = Logger.getLogger(BaseServlet.class);

    public BaseServlet() {

    }

    protected void out(String content, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (resp.getStatus() != HttpServletResponse.SC_MOVED_TEMPORARILY && resp.getStatus() != HttpServletResponse.SC_MOVED_PERMANENTLY) {
            setDefaultHeader(resp);
            PrintWriter out = resp.getWriter();

            try {

                out.println(content);

            } catch (Exception ex) {
                _log.error(ex.getMessage(), ex);
            } finally {
                out.flush();
                out.close();
            }
        }
    }

    protected void setDefaultHeader(HttpServletResponse resp) {
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=UTF-8");
        resp.setHeader("Vary", "Accept-Encoding");
        resp.setHeader("P3P", "CP='NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM'");
    }
}