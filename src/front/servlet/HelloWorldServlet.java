package front.servlet;

import entity.UserEntity;
import model.ModelUser;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by thinh on 8/30/2016.
 */
public class HelloWorldServlet extends BaseServlet {
    private static final Logger logger = Logger.getLogger(HelloWorldServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            logger.info("hello world");
            JSONArray ja = new JSONArray();
            JSONObject json = new JSONObject();

//            Map<String, UserEntity> users = ModelUser.getInstance().getMapActiveUsers();
//            for(String key : users.keySet()) {
//                json.put("username", users.get(key).userName);
//            }
            json.put("username", "hello");
            ja.add(json);
            String content = ja.toJSONString();
            this.out(content, req, resp);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }
}
