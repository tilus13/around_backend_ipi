package httpservice;

import org.apache.log4j.Logger;
import utils.LogUtil;

import java.io.File;

public class ServiceDaemon {

    private static final Logger logger_ = Logger.getLogger(ServiceDaemon.class);

    public static void main(String[] args) throws Exception {

        LogUtil.init();
        ApiServer apiServer = new ApiServer();
        String pidFile = System.getProperty("pidfile");


        try {
            if (pidFile != null) {
                new File(pidFile).deleteOnExit();
            }
            if (System.getProperty("foreground") == null) {
                System.out.close();
                System.err.close();
            }

            //warm up local cached
//            GuavaLocalCachedTemplate.getInstance().get(GuavaLocalCachedKey.getListGameHtmlKey());
//            GuavaLocalCachedTemplate.getInstance().get(GuavaLocalCachedKey.getHourlyNewRoleKey());
            //stats
//            InitAndMonitor.init();
            apiServer.start();

        } catch (Throwable e) {
            String msg = "Exception encountered during startup.";
            logger_.error(msg, e);

            // try to warn user on stdout too, if we haven't already detached
            System.out.println(msg);
            logger_.error("Uncaught exception: " + e.getMessage());

            System.exit(3);
        }
    }
}
