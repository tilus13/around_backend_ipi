package httpservice;

import front.servlet.HelloWorldServlet;
import front.utils.ConnectionManager;
import org.apache.log4j.Logger;
import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlets.GzipFilter;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import utils.DBPool;

import javax.servlet.DispatcherType;
import java.lang.management.ManagementFactory;
import java.util.EnumSet;
import utils.ConvertUtils;

public class ApiServer {
    public void start() throws Exception {
        DBPool.getInstance();

        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(100);
        threadPool.setMaxThreads(2000);

        Server server = new Server(threadPool);

        // Setup JMX
        MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
        server.addBean(mbContainer);

        String host_listen = System.getProperty("zhost");
        int port_listen = ConvertUtils.toInt(System.getProperty("zport"));

        HttpConfiguration httpConf = new HttpConfiguration();
        httpConf.setSendServerVersion(false);

        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(httpConf));
        connector.setHost(host_listen);
        connector.setPort(port_listen);

        server.setConnectors(new Connector[]{connector});

        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(HelloWorldServlet.class, "/hello");

//        handler.addFilter(AuthenticationFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC));
        handler.addFilter(GzipFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC));

        HandlerCollection handlers = new HandlerCollection();

        handlers.addHandler(handler);

        server.setHandler(handlers);
        server.setStopAtShutdown(true);

        Runtime.getRuntime().addShutdownHook(new ShutdownThread(server));

        ShutdownThread obj = new ShutdownThread(server);
        Runtime.getRuntime().addShutdownHook(obj);
        try {
            server.start();
            server.join();
        } catch (Exception ex) {
        }
    }
}

class ShutdownThread extends Thread {

    private final Server server;
    private static final Logger logger_ = Logger.getLogger(ShutdownThread.class);

    public ShutdownThread(Server server) {
        this.server = server;
    }

    @Override
    public void run() {
        logger_.info("Waiting for shut down!");
        try {
            ConnectionManager.getInstance().shutdownConnPool();

            server.stop();
        } catch (Exception ex) {
            logger_.error(ex.getMessage());
        }
        logger_.info("Server shutted down!");
    }
}
