package utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ConvertUtils {
    public ConvertUtils() {
    }

    public static float toFloat(Object value) {
        return toFloat(value, 0.0F);
    }

    public static float toFloat(Object value, float def) {
        if(value == null) {
            return def;
        } else if(value instanceof Number) {
            return ((Number)value).floatValue();
        } else {
            try {
                return Float.parseFloat(value.toString());
            } catch (Exception var3) {
                return def;
            }
        }
    }

    public static double toDouble(Object value) {
        return toDouble(value, 0.0D);
    }

    public static double toDouble(Object value, double def) {
        if(value == null) {
            return def;
        } else if(value instanceof Number) {
            return ((Number)value).doubleValue();
        } else {
            try {
                return Double.parseDouble(value.toString());
            } catch (Exception var4) {
                return def;
            }
        }
    }

    public static byte toByte(String value) {
        return toByte(value, Byte.parseByte("0"));
    }

    public static byte toByte(String value, byte defaultValue) {
        if(ValidationUtils.isNumber(value)) {
            try {
                return Byte.parseByte(value);
            } catch (Exception var3) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public static short toShort(String value) {
        return toShort(value, Short.parseShort("0"));
    }

    public static short toShort(Object value) {
        return value != null?toShort(value.toString(), Short.parseShort("0")):0;
    }

    public static short toShort(String value, short defaultValue) {
        if(ValidationUtils.isNumber(value)) {
            try {
                return Short.parseShort(value);
            } catch (Exception var3) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public static int toInt(Object value) {
        return toInt(value, 0);
    }

    public static int toInt(Object value, int def) {
        if(value == null) {
            return def;
        } else if(value instanceof Number) {
            return ((Number)value).intValue();
        } else {
            try {
                return Integer.parseInt(value.toString());
            } catch (Exception var3) {
                return def;
            }
        }
    }

    public static long toLong(Object value) {
        return toLong(value, 0L);
    }

    public static long toLong(Object value, long def) {
        if(value == null) {
            return def;
        } else if(value instanceof Number) {
            return ((Number)value).longValue();
        } else {
            try {
                return Long.parseLong(value.toString());
            } catch (Exception var4) {
                return def;
            }
        }
    }

    public static String toString(Object obj) {
        return toString(obj, "");
    }

    public static String toString(Object obj, String defaultValue) {
        try {
            return obj == null?defaultValue:(obj instanceof Date ?toString((Date)obj):obj.toString());
        } catch (Exception var3) {
            return defaultValue;
        }
    }

    public static boolean toBoolean(Object obj) {
        return toBoolean(obj, false);
    }

    public static boolean toBoolean(Object obj, boolean defaultValue) {
        try {
            if(obj != null) {
                if(obj instanceof Boolean) {
                    return ((Boolean)obj).booleanValue();
                }

                if(obj instanceof String) {
                    return Boolean.parseBoolean((String)obj);
                }

                if(ValidationUtils.isNumber(obj)) {
                    return obj.equals(Integer.valueOf(1));
                }
            }
        } catch (Exception var3) {
            ;
        }

        return defaultValue;
    }

    public static List<String> convertListLong(List<Long> values) {
        ArrayList result = new ArrayList();
        if(values != null && !values.isEmpty()) {
            Iterator i$ = values.iterator();

            while(i$.hasNext()) {
                Long value = (Long)i$.next();
                result.add(String.valueOf(value));
            }

            return result;
        } else {
            return result;
        }
    }

    public static String[] List2Array(List<Long> values) {
        if(values != null && !values.isEmpty()) {
            String[] result = new String[values.size()];

            for(int i = 0; i < values.size(); ++i) {
                result[i] = String.valueOf(values.get(i));
            }

            return result;
        } else {
            return new String[0];
        }
    }

    public static List<Long> convertListString(List<String> values) {
        ArrayList result = new ArrayList();
        if(values != null && !values.isEmpty()) {
            Iterator i$ = values.iterator();

            while(i$.hasNext()) {
                String value = (String)i$.next();
                String tmp = value.trim();
                result.add(Long.valueOf(toLong(tmp, 0L)));
            }

            return result;
        } else {
            return result;
        }
    }

    public static String decodeString(byte[] data) {
        String rv = null;

        try {
            if(data != null) {
                rv = new String(data, "UTF-8");
            }

            return rv;
        } catch (UnsupportedEncodingException var3) {
            throw new RuntimeException(var3);
        }
    }

    public static byte[] encodeString(String in) {
        Object rv = null;

        try {
            byte[] rv1 = in.getBytes("UTF-8");
            return rv1;
        } catch (UnsupportedEncodingException var3) {
            throw new RuntimeException(var3);
        }
    }
}
