/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import org.apache.log4j.Logger;
import snaq.db.ConnectionPoolManager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author thinh
 */
public class DBPool {

    protected Connection conn;
    protected ConnectionPoolManager connManager;
    private static DBPool dbPool;
    private static final Logger _log = Logger.getLogger(DBPool.class);

    private static final String databaseName = "pool-mysql";

    public DBPool() {
        try {

            String host = Config.getParam("mysql", "host");
            String port = Config.getParam("mysql", "port");
            String username = Config.getParam("mysql", "user");
            String password = Config.getParam("mysql", "password");
            String database = Config.getParam("mysql", "dbname");
            String idleTimeOut = "3600";
            String maxPool = "50";
            String minPool = "5";
            String maxSize = "100";

            Properties p = new Properties();
            p.setProperty("name", "pool-mysql");
            p.setProperty("drivers", "com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + host + ":" + port + "/" + database + "?useUnicode=true&characterEncoding=UTF-8";
            p.setProperty("pool-mysql.url", url);
            p.setProperty("pool-mysql.user", username);
            p.setProperty("pool-mysql.password", password);
            p.setProperty("pool-mysql.validator", "snaq.db.AutoCommitValidator");
            p.setProperty("pool-mysql.minpool", minPool);
            p.setProperty("pool-mysql.maxpool", maxPool);
            p.setProperty("pool-mysql.maxsize", maxSize);
            p.setProperty("pool-mysql.idleTimeout", idleTimeOut);

            ConnectionPoolManager.createInstance(p);

            connManager = ConnectionPoolManager.getInstance();

        } catch (IOException ex) {
            _log.info("Error While Connecting with DBPool Properties file :=> " + ex.toString());
        }
    }

    /**
     * Creates/Provides the instance of the Pool.
     *
     * @return DBPool
     */
    public static DBPool getInstance() {

        if (dbPool == null) {
            //System.out.println("dbPool null");
            dbPool = new DBPool();
        }
        // System.out.println("call dbpool");
        return dbPool;
    }

    public static void shuttdownPool() {
        try {
            if (dbPool != null) {
                dbPool.connManager.removeShutdownHook();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Sets the connection object.
     *
     */
    public Connection getConn() {
        Connection con = null;
        try {

            con = connManager.getConnection(databaseName);
            // System.out.println("pool infor-----"+connManager.getPool(databaseName).toString());
            connManager.getPool(databaseName);
            //System.out.println("call getConn : ----------" + con.toString());
//            if(con.isClosed()){
//                logger.info("connection is closed ...");
//            }
            //logger.info("Connection Created: " + con.toString());
        } catch (SQLException ex) {
            _log.info("Error While Creating Connection :=> " + ex.toString());
        }
        if (con != null) {
            this.conn = con;
            //logger.info("Connection Released: " + this.conn.toString());
            return con;
        } else {
            return con;
        }
    }
}
