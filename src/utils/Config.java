package utils;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created by CPU10323-local on 9/1/2016.
 */
public class Config {
    public static final String CONFIG_HOME = "conf";
    public static final String CONFIG_FILE = "config.ini";
    private static Logger logger = Logger.getLogger(Config.class);
    static CompositeConfiguration config;

    public Config() {
    }

    public static String getHomePath() {
        return System.getProperty("apppath");
    }

    public static String getParam(String section, String name) {
        String key = section + "." + name;
        String value = config.getString(key);
//        String value = (String)localconfig.get(key);
//        if(value != null) {
//            return value;
//        } else {
//            value = config.getString(key);
//            if(value != null) {
//                localconfig.put(key, value);
//            }

        return value;
//        }
    }

    static {
        String CONFIG_ITEMS = System.getProperty("cfg_items");
        String HOME_PATH = System.getProperty("apppath");
        String APP_ENV = System.getProperty("appenv");
        if(CONFIG_ITEMS == null || CONFIG_ITEMS.equals("")) {
            CONFIG_ITEMS = "500";
        }

        if(APP_ENV == null) {
            APP_ENV = "";
        }

        if(APP_ENV != "") {
            APP_ENV = APP_ENV + ".";
        }

//        localconfig = new InstrumentedCache(Integer.valueOf(CONFIG_ITEMS).intValue());
        config = new CompositeConfiguration();
        File configFile = new File(HOME_PATH + File.separator + "conf" + File.separator + APP_ENV + "config.ini");

        logger.info(HOME_PATH + File.separator + "conf" + File.separator + APP_ENV + "config.ini");

        try {
            config.addConfiguration(new HierarchicalINIConfiguration(configFile));
//            Iterator e = config.getKeys();

//            while(e.hasNext()) {
//                String key = (String)e.next();
//                localconfig.put(key, config.getString(key));
//            }
        } catch (ConfigurationException var6) {
            System.out.println("Exception when Config");
            System.exit(1);
        }

    }
}
