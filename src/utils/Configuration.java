/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author thinh
 */
public class Configuration {

    public static final int ACTIVED_USER = 0;
    public static final int NORMAL_USER = 0;
    public static final int LOCKED_USER = 1;

    public static final int ADMIN_USER = 1;
    public static final int SUPER_ADMIN_USER = 2;

    public static String CONTEXT_PATH;
    public static String STATIC_URL;
    public static String ROOT_URL;
    public static String API_PRIVATE_KEY_LOGIN = "a6e62390243649d58fbe55ddc2f464a7";

    public static String _MYSQL_DATABASE_NAME;

    public static String _REDIS_SERVER_HOST;
    public static int _REDIS_SERVER_PORT;
    
    public static final Map<String, String> mapSourceRealtime = new LinkedHashMap<>();
    
    public static final List<String> list_game_siam = new ArrayList<>();
    
    public static List<String> mapAppNameSub = new ArrayList<>();

    static {
        _MYSQL_DATABASE_NAME = Config.getParam("mysql", "dbname");
        CONTEXT_PATH = Config.getParam("url", "context-path");
        ROOT_URL = Config.getParam("url", "root-url");
        STATIC_URL = Config.getParam("url", "static-url");

        _REDIS_SERVER_HOST = Config.getParam("dashboard_redis_server", "host");
        _REDIS_SERVER_PORT = ConvertUtils.toInt(Config.getParam("dashboard_redis_server", "port"));
        
    }

//    public static List<UserProfileEntity> listAllowUser = ModelUser.getInstance().getListActiveUsers();
//    public static Map<String, UserProfileEntity> mapAllowUser = ModelUser.getInstance().getMapActiveUsers();
//
//    public static Map<String, ProductMapEntity> mapAppName = new TreeMap<>(ModelMobileProductMap.getInstance().getListAppAndGameNameEntity());
    
    
    
}
